require('dotenv').config();
var express = require('express'),
app = express(), 
logger = require('morgan'),
bodyParser = require('body-parser'),
errorHandler = require('errorhandler'), 
mongoDB = require('./config/db'),
Router = express.Route()
expressValidator = require("express-validator")
port = process.env.PORT || 8000;

if(process.env.ENV == "development"){
    app.use(errorHandler())
    // console.log('Development Only')
    console.log(process.env.NODE_ENV)
}else{
    console.log(process.env.NODE_ENV)
    // console.log('Production or Live Only')
}

app.use(logger('dev'))
app.use(bodyParser.json())
app.use(express.urlencoded({extended:true}))
// app.use(expressVa//lidator())

////////// Mongo DB Connection Checker ////////////
mongoDB()

/////////////// Controller Section ////////////////
const api = require("./routes/api")

////////////// Route Section ////////////////////// 
app.use('/api', api)



//////////// Server Section /////////////////////
app.listen(port, ()=>{
    console.log('App listening on port '+port+'!')
})