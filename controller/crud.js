const db = require('../models/user_details'),
auth = require('./auth'),
bcrypt = require('bcrypt')
exports.getData = (req, res)=>{
    // return res.status(200).json()
    let data = JSON.parse(JSON.stringify(req.body))
    
    if(data.paramData === 1){
        return results = db.findOne({accountNumber : data.id_number})
        .then((data)=>{
            console.log("catch Data from Acc Number")
            console.log(data)
            if(data){
                let all = {
                    "error" : false,
                    "message" : "Data Found From id_number",
                    "data" : [{
                        "username" : data.userName,
                        "accountNumber" : data.accountNumber,
                        "email" : data.email,
                        "identityNumber" : data.identityNumber,
                    }]
                }
                return res.status(200).json(all)
            }else{
                let all = {
                    "error" : true,
                    "message" : "Data Not Found From id_number",
                    "data" : null
                }
                return res.status(404).json(all)
            }
        })
        .catch((err)=>{
            console.log('error')
            console.log(err)
        })
    }else if (data.paramData === 2){
        return db.findOne({identityNumber : data.id_number})
        .then((data)=>{
            console.log("catch Data from id number")
            console.log(data)
            if(data){
                let all = {
                    "error" : false,
                    "message" : "Data Found From id_number",
                    "data" : [{
                        "username" : data.userName,
                        "accountNumber" : data.accountNumber,
                        "email" : data.email,
                        "identityNumber" : data.identityNumber,
                    }]
                }
                return res.status(404).json(all)
            }else{
                let all = {
                    "error" : true,
                    "message" : "Data Not Found From id_number",
                    "data" : null
                }
                return res.status(200).json(all)
            }
        })
        .catch((err)=>{
            console.log('error')
            console.log(err)
        })
    }
}
exports.insert = (req,res) =>{
    var body = JSON.parse(JSON.stringify(req.body))
    // console.log(body)

    //Salt Sync Bcrypt
    const saltRounds = bcrypt.genSaltSync(10)

    // Formater Data Push
    let request_data = {
       "data_login" :{
        "userName" : body.username,
        "password" : bcrypt.hashSync(body.password, saltRounds),
        "accountNumber" : body.acc_number,
        "email" : body.emailAddr,
        "identityNumber" : body.ident_number
       }
    }
    //Respone From Data
    return res.status(200).json(auth.register(request_data))
    // return res.status(200).json(request_data)
}
exports.update = (req, res) =>{
    let data = JSON.parse(JSON.stringify(req.body))
    return db.findOne({_id: data.id})
    .then((datas)=>{
        if(datas){
            return db.save((err,user)=>{
                if(err) return console.log(err)
                return res.status(200).json({ "FeedBack:" :user.userName, "Status": "Saved"})
            })
        }
    })
    // register.save((err, User)=>{
    //     if(err) return console.log(err)
    //     return { "FeedBack":User.userName + "Has Saved"}
    // })
}
exports.delete = ()=>{

}