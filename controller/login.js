'use strict'
const {validationResult } = require('express-validator');
const genToken = require('../config/jwt_token')
const bcrypt = require('bcrypt') 
var auth = require('./auth') 
exports.auth = (req,res)=>{ 
    const error = validationResult(req) 
    const data = JSON.parse(JSON.stringify(req.body))
    // Validation Respone
    if(!error.isEmpty()){
        return res.status(400).json({errors: error.array()})
    }  
    console.log('object :', data);
    let hash_password = data.password,
    push_data = {
        "username": data.username
    }
    // Get Data Database
    auth.auth(push_data)
    .then((getData)=>{  
        
    if(getData != null){
        console.log(getData.id)
         // Respone Password Validation
        if (bcrypt.compareSync(hash_password, getData.password))
        { 
        // Respone Data if True password
            var result = {
            "error" : false,
            "message" : "Data Found",
            "data" : [{
                "token": genToken.generate(getData.id),
                "details" :[{
                    "username" : getData.userName,
                    "Account_Number": getData.accountNumber,
                    "email" : getData.email,
                    "identity_Number" : getData.identityNumber
                }]
            }]
        }
       res.status(200).json(result) 
    }else{
            //respone if false password
           res.status(400).json({
            "error" : true,
            "message" : "Wrong Password",
            "data" : null
           })
       }
    }else{
        // Respone Fail
        res.status(400).json({
            "error": true,
            "message" : "Username wrong",
            "data": null
        })
    }
    }) 
}

exports.register = (req,res) =>{
    var body = JSON.parse(JSON.stringify(req.body))
    // console.log(body)

    //Salt Sync Bcrypt
    const saltRounds = bcrypt.genSaltSync(10)

    // Formater Data Push
    let request_data = {
        // "created_By" : {
        //     "id" : body.registerBy.id,
        //     "access_id" :body.registerBy.access_id
        //  },
       "data_login" :{
        "userName" : body.username,
        "password" : bcrypt.hashSync(body.password, saltRounds),
        "accountNumber" : body.acc_number,
        "email" : body.emailAddr,
        "identityNumber" : body.ident_number
       }
    }
    //Respone From Data
    return res.status(200).json(auth.register(request_data))
    // return res.status(200).json(request_data)
}