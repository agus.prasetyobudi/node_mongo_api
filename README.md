##### Node Mongo API ######
- Express JS
- Mongoose
- dotENV
- Morgan
- ErrorHandler
- BodyParser
- path
- Redis
- SuperAgent

Access List Link API : 
username : admin
password : passwords

Status Server : http://jenius-api.agusprasetyo.web.id/info  Method : GET
Login User    : http://jenius-api.agusprasetyo.web.id/login Method : POST
getData User   : http://jenius-api.agusprasetyo.web.id/get/data Method : POST With Token : Bearer param
Insert User   : http://jenius-api.agusprasetyo.web.id/insert Method : POST With Token : Bearer
Update User   : http://jenius-api.agusprasetyo.web.id/update Method : POST With Token : Bearer (Comming Soon)
Delete User   : http://jenius-api.agusprasetyo.web.id/delete Method : POST With Token : Bearer (Comming Soon)

Link_Type : getData 
Body : Json 
Param : 
By id_number:
{
	"paramData" : 1, // Param Data 1 as Account Number and Param Data 2 as Identity Number
	"id_number"	: "",
} 
