require('dotenv').config();
const jwt = require('jsonwebtoken')

exports.generate = (data)=>{
return jwt.sign({
    id:data.id},
    process.env.APP_KEY,
    {expiresIn: '1h'}
    )
}

exports.validate = (req, res, next)=>{
    let token = req.headers.authorization
    if (token.startsWith('Bearer ')) {
        token = token.slice(7, token.length)
    }
    jwt.verify(token,
        process.env.APP_KEY, (err, decode) =>{
        // let err = true
        console.dir({token})
        if(err){
                res.status(400).json({
                    "Error" : true,
                    "message" : err.message,
                    "data" : null
                })
            }else{
                req.body.id = decode.id
                // console.log('throw Data '+decode)
                console.dir({
                    'decode': decode
                })
                next()
            }
        }
    )
}