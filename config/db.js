require('dotenv').config();
const mongoose = require('mongoose'),
env = process.env
host = env.DB_TYPE+'://'+env.DB_HOST+'/'+env.DB_NAME
// host = 'mongodb://agusprasetyo.web.id:27017/JeniusTest'

module.exports = ()=>{
    try {
        mongoose.connect(host, { useNewUrlParser: true, useUnifiedTopology: true })
    } catch (error) {
        handleError(error)
    }
}