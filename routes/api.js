const express = require('express'),
Routes = express.Router(),
login = require('../controller/login'),
crud = require('../controller/crud'),
jwt = require('../config/jwt_token')
const {body} = require('express-validator')

Routes.post('/login',[
   body('username').not().isEmpty().withMessage('username required'),
   body('password').not().isEmpty()
],login.auth)
// Routes.post('/login',[
// //    body('username').not().isEmpty().withMessage('username required'),
// //    body('password').not().isEmpty()
// ],login.ids)
Routes.post('/get/data',jwt.validate,crud.getData)
Routes.post('/insert',jwt.validate, crud.insert)
Routes.post('/update',jwt.validate, crud.update)
Routes.post('/delete',[
	body('username').not().isEmpty().withMessage('username required'),
	body('password').not().isEmpty().withMessage('password required'),
	body('first_name').not().isEmpty().withMessage('first name required'),
	body('last_name').not().isEmpty().withMessage('last name required')

],login.register)

Routes.get('/info',(req, res)=>{
	res.status(200).json({
	 error : null,
	 status :[{
		 server : "OK",
		}]
	})
})


module.exports = Routes
