'use strict';
const mongoose = require('mongoose'),
schema = mongoose.Schema

var userDetailSchema = new schema({
  userName : {
    type: String,
    required: true
  },
  password:{
    type: String,
    required: true
  },
  accountNumber : {
    type: Number,
    required: true
  },
  email : {
    type:String,
    required: true
  },
  identityNumber: {
    type: Number,
    required: true
  },
  createdAt : {type:Date, default: Date.now}
})

module.exports = mongoose.model('User', userDetailSchema)
